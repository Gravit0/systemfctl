#include <iostream>
#include <fstream>
#ifndef BINDIR
#define BINDIR "/usr/bin/"
#endif
#ifndef UNITDIR
#define UNITDIR "/usr/lib/systemd/system/"
#endif
using namespace std;
void killfromstart(std::string execstart)
{
    std::string nullcommand;
    auto i1 = execstart.find(' ');
    if(i1>0)
        nullcommand=execstart.substr(0,i1);
    else nullcommand=execstart;
    std::string lastcmd;
    auto i2 = nullcommand.find_last_of('/');
    if(i2>0)
        lastcmd=nullcommand.substr(i2+1, nullcommand.size());
    else lastcmd=nullcommand;
    system((BINDIR "pkill "+lastcmd).c_str());
}
bool StartUnit(std::fstream* file)
{

    if (!(*file)) { std::cout << "err\n";  return false;}
    while((*file))
    {
        std::string str;
        //file >> str;
        getline(*file,str,'\n');
        if(str=="[Service]" || str=="[Service]\r")
        {
            while((*file))
            {
                std::string str2;
                getline(*file,str2,'\n');
                auto i = str2.find('=');
                std::string first = str2.substr(0,i);
                if(first=="ExecStart")
                {
                    std::string secound = str2.substr(i+1,str2.size());
                    std::cout << secound << "\n";
                    system((BINDIR "bash -c \" " + secound + "\" &").c_str());
                    return true;
                    break;
                }
            }
            break;
        }
    }
    return false;
}
bool ReloadUnit(std::fstream* file)
{
    if (!(*file)) { std::cout << "err\n";  return false;}
    while((*file))
    {
        std::string str;
        //file >> str;
        getline(*file,str,'\n');
        if(str=="[Service]" || str=="[Service]\r")
        {
            while((*file))
            {
                std::string str2;
                getline(*file,str2,'\n');
                auto i = str2.find('=');
                std::string first = str2.substr(0,i);
                if(first=="ExecReload")
                {
                    std::string secound = str2.substr(i+1,str2.size());
                    std::cout << secound << "\n";
                    system((BINDIR "bash -c \" " + secound + "\" &").c_str());
                    return true;
                    break;
                }
            }
            break;
        }
    }
    return false;
}
bool StopUnit(std::fstream* file)
{
    if (!(*file)) { std::cout << "err\n"; return false;}
    while((*file))
    {
        std::string str;
        //file >> str;
        getline(*file,str,'\n');
        if(str=="[Service]" || str=="[Service]\r")
        {
            std::string execstop,execstart,killmode;
            while((*file))
            {
                std::string str2;
                getline(*file,str2,'\n');
                auto i = str2.find('=');
                std::string first = str2.substr(0,i);

                bool startkillmode = false;
                if(first=="ExecStop")
                {
                    std::string secound = str2.substr(i+1,str2.size());
                    execstop=secound;
                    if(killmode=="mixed")
                    {
                        system(secound.c_str());
                        break;
                    }
                }
                else if(first=="ExecStart")
                {
                    std::string secound = str2.substr(i+1,str2.size());
                    execstart=secound;
                    if(startkillmode)
                    {
                        killfromstart(secound);
                        break;
                    }
                }
                else if(first=="KillMode")
                {
                    std::string secound = str2.substr(i+1,str2.size());
                    killmode=secound;
                    if(secound=="mixed" && (!execstop.empty()))
                    {
                        system(execstop.c_str());
                        break;
                    }
                    else if(secound=="process")
                    {
                        if(execstart.empty()) startkillmode=true;
                        else
                        {
                            killfromstart(execstart);
                            break;
                        }

                    }
                }
            }
            break;
        }
    }
}
void PrintHelp()
{
    std::cout << "start unitname - start Systemd Unit\n";
    std::cout << "restart unitname - restart Systemd Unit\n";
    std::cout << "reload unitname - sending service configuration restart signal\n";
    std::cout << "stop unitname - stop Systemd Unit\n";
    std::cout << "help - show the current text\n";
    std::cout << "This program is not a real systemd\n"
                 "Dependencies are not supported!\n";
}

int main(int argc, char *argv[])
{
    if(argc<=1)
    {
        PrintHelp();
        return 0;
    }
    std::string cmd(argv[1]);
    if(cmd=="start")
    {
        std::string unitname(argv[2]);
        std::string filename = UNITDIR+unitname+".service";
        std::fstream file;
        file.open(filename, ios_base::in);
        StartUnit(&file);
        file.close();
    }
    else if(cmd=="stop")
    {
        std::string unitname(argv[2]);
        std::string filename = UNITDIR+unitname+".service";
        std::fstream file;
        file.open(filename, ios_base::in);
        StopUnit(&file);
        file.close();
    }
    else if(cmd=="reload")
    {
        std::string unitname(argv[2]);
        std::string filename = UNITDIR+unitname+".service";
        std::fstream file;
        file.open(filename, ios_base::in);
        ReloadUnit(&file);
        file.close();
    }
    else if(cmd=="restart")
    {
        std::string unitname(argv[2]);
        std::string filename = UNITDIR+unitname+".service";
        std::fstream file;
        file.open(filename, ios_base::in);
        StopUnit(&file);
        file.seekg(0);
        file.seekp(0);
        StartUnit(&file);
        file.close();
    }
    else if(cmd=="help")
    {
        PrintHelp();
    }
    return 0;
}
